module codeberg.org/Codeberg/pages

go 1.13

require (
	github.com/Microsoft/go-winio v0.4.14 // indirect
	github.com/buger/jsonparser v1.0.0
	github.com/docker/distribution v2.7.1+incompatible // indirect
	github.com/docker/docker v1.13.1
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/docker/go-units v0.4.0 // indirect
	github.com/markbates/pkger v0.17.0
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/opencontainers/go-digest v1.0.0-rc1 // indirect
	github.com/pkg/errors v0.8.1
	github.com/rs/zerolog v1.19.0
	github.com/sergi/go-diff v1.1.0 // indirect
	github.com/valyala/fasthttp v1.14.0
	golang.org/x/crypto v0.0.0-20200403201458-baeed622b8d8 // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/src-d/go-billy.v4 v4.3.2
	gopkg.in/src-d/go-git.v4 v4.13.1
)
