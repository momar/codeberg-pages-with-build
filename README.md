# Codeberg Pages

This is a standalone reimplementation of Codeberg Pages in pure Go,
which can be used with any Git-based hosting solution that offers a
JSON-based API.

It's currently under heavy development and not yet ready for production
use.
