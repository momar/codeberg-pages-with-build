package pages

import (
	"github.com/rs/zerolog/log"
	"time"
)

var RepositoryHashURL = "https://codeberg.org/api/v1/repos/%u/%r/git/refs/heads/%b"
var RepositoryHashQuery = []string{"[0]", "object", "sha"}
var RepositoryBranchURL = "https://codeberg.org/api/v1/repos/%u/%r"
var RepositoryBranchQuery = []string{"default_branch"}
var RepositoryFormat = "https://codeberg.org/%u/%r.git"

var BuildRoot = "/var/codeberg-pages"
var BuildTimeout = 500 * time.Second

var Domain = "codeberg.eu"

func Setup() {
	if err := LoadCache(); err != nil {
		log.Warn().Err(err).Msg("couldn't load build cache")
	} else {
		log.Debug().Msgf("build cache loaded (currently %d repositories)", len(buildCache))
	}

	go func() {
		for {
			time.Sleep(15 * time.Minute)

			// Cleanup every 15 minutes
			Cleanup()

			// Save build cache after cleanup
			if err := SaveCache(); err != nil {
				log.Warn().Err(err).Msg("couldn't save build cache")
			} else {
				log.Debug().Msgf("build cache saved (currently %d repositories)", len(buildCache))
			}
		}
	}()
}
