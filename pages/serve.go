package pages

import (
	"bufio"
	"bytes"
	"encoding/json"
	"io/ioutil"
	"mime"
	"net"
	"os"
	"path"
	"strconv"
	"strings"
	"time"

	"github.com/markbates/pkger"
	"github.com/valyala/fasthttp"
)

func init() {
	pkger.Include("/html/...")
}

// Serve starts a web server that serves the static files,
// automatically schedules builds & provides the build status API.
func Serve() error {
	// This code is mostly taken from https://codeberg.org/momar/web/src/branch/master/main.go.
	fsHandler := (&fasthttp.FS{
		Root:               path.Join(BuildRoot, "output"),
		GenerateIndexPages: false,
		Compress:           true,
		AcceptByteRange:    true,
		IndexNames:         []string{"index.html", "index.htm"},
		PathNotFound: func(ctx *fasthttp.RequestCtx) {
			ctx.Response.SetStatusCode(404)
		},
	}).NewRequestHandler()
	addr := os.Getenv("HOST") + ":" + os.Getenv("PORT")
	if strings.HasPrefix(addr, ":") {
		addr = "[::]" + addr
	} else if strings.HasSuffix(addr, ":") {
		addr += ":80"
	}
	listener, err := net.Listen("tcp", addr)
	if err != nil {
		return err
	}
	return (&fasthttp.Server{
		GetOnly: true,
		Logger:  disabledLogger{},
		Handler: func(ctx *fasthttp.RequestCtx) {
			// resolve subdomain
			var userName, repoName, prefix string
			originalPath := string(ctx.Path())
			parts := bytes.SplitN(ctx.Host(), []byte("."), 2)
			if len(parts) > 1 && !bytes.Equal(parts[1], []byte(Domain)) {
				ctx.Error("Invalid Host header " + string(parts[1]), 400)
				// TODO: resolve & confirm domain
				return
			} else if len(parts) <= strings.Count(Domain, ".") + 1 {
				userName = ""
				repoName = ""
			} else {
				userName = string(parts[0])
				repoName = string(bytes.SplitN(bytes.Trim(ctx.Path(), "/"), []byte("/"), 2)[0])
				if repoName == "" {
					repoName = "pages"
				} else if _, hash := HashRepository(userName, repoName); hash == "" {
					repoName = "pages"
				} else {
					uri := bytes.SplitN(bytes.TrimPrefix(ctx.Path(), []byte("/")), []byte("/"), 2)
					if len(uri) > 1 {
						ctx.Request.SetRequestURI("/" + string(uri[1]))
					} else {
						ctx.Redirect(string(ctx.Path()) + "/", fasthttp.StatusFound)
						return
					}
				}
			}
			var info *BuildInfo
			if userName != "" {
				info = Update(userName, repoName)
				// wait until RequiresBuild is set
				for info != nil && !info.Done && info.RequiresBuild == nil {
					time.Sleep(25 * time.Millisecond)
				}
				// wait until move is completed (no build required)
				for info != nil && (info.RequiresBuild == nil || !*info.RequiresBuild) && !info.Done {
					time.Sleep(25 * time.Millisecond)
				}
				if info != nil {

					prefix = strings.TrimRight("/"+userName+"/"+repoName, "/")
					ctx.Request.SetRequestURI(prefix + string(ctx.RequestURI()))

					if bytes.HasSuffix(ctx.RequestURI(), []byte("/.log.stream")) {
						ctx.Response.Header.SetContentType("text/event-stream")
						ctx.Response.Header.Set("Cache-Control", "no-cache")
						sw := func(w *bufio.Writer) {
							streamed, _ := strconv.Atoi(string(ctx.Request.Header.Peek("Last-Event-ID")))
							w.Write([]byte("event: ready\ndata: {\"skipped\":" + strconv.Itoa(streamed) + "}\nretry: 500\n\n"))
							w.Flush()
							for {
								part := info.Log[streamed:]
								streamed += len(part)
								partJson, _ := json.Marshal(part)
								w.Write([]byte("event: message\nid: " + strconv.Itoa(streamed) + "\ndata: "))
								w.Write(partJson)
								w.Write([]byte("\n\n"))
								w.Flush()
								info.LogLock.RLock()
								info.LogLock.RUnlock()
								if info.Done {
									break
								}
							}

							if info.Error != nil {
								w.WriteString("event: failed\ndata: \n\n")
							} else {
								w.WriteString("event: success\ndata: \n\n")
							}
							w.Flush()
						}
						ctx.SetBodyStreamWriter(sw)
					} else if !info.Done || info.Error != nil || bytes.HasSuffix(ctx.RequestURI(), []byte("/.log")) {
						ctx.Response.Header.Set("Cache-Control", "no-cache")
						file, err := pkger.Open("/html/build.html")
						if err != nil {
							ctx.Error("File not found", 404)
							return
						}
						c, _ := ioutil.ReadAll(file)
						t := "loading"
						if info.Error != nil {
							t = "error"
						}
						c = bytes.ReplaceAll(c, []byte("%%TYPE%%"), []byte(t))
						c = bytes.ReplaceAll(c, []byte("%%USER%%"), []byte(info.Owner))
						c = bytes.ReplaceAll(c, []byte("%%REPO%%"), []byte(info.Repository))
						ctx.Response.Header.SetContentType("text/html; charset=utf-8")
						ctx.Write(c)
					} else {
						// serve actual file
						ctx.Response.Header.Set("Cache-Control", "must-revalidate")
						fsHandler(ctx)
						if ctx.Response.StatusCode() == fasthttp.StatusFound && !strings.HasSuffix(originalPath, "/") {
							ctx.Redirect(originalPath+"/", fasthttp.StatusFound)
						}
					}
				} else {
					ctx.SetStatusCode(404)
				}
			} else {
				requestPath := string(ctx.Path())
				contentType := mime.TypeByExtension(path.Ext(requestPath))
				file, err := pkger.Open("/html" + requestPath)
				if err != nil {
					ctx.Error("File not found", 404)
				} else {
					info, err := file.Stat()
					if err == nil && info.IsDir() {
						if !strings.HasSuffix(string(ctx.Path()), "/") {
							ctx.Redirect(string(ctx.Path()) + "/", fasthttp.StatusFound)
							return
						} else {
							file, err = pkger.Open("/html" + strings.TrimSuffix(string(ctx.Path()), "/") + "/index.html")
							contentType = "text/html; charset=utf-8"
						}
					}

					c, _ := ioutil.ReadAll(file)
					ctx.Response.Header.SetContentType(contentType)
					ctx.Response.Header.SetContentLength(len(c))
					_, _ = ctx.Write(c)
				}
			}

			if ctx.Response.StatusCode() > 399 { // serve not found page for all request/server errors
				if prefix != "" {
					ctx.Request.Reset()
					ctx.Response.Reset()
					ctx.Request.SetRequestURI(prefix + "/404.html")
					fsHandler(ctx)
				}
				if ctx.Response.StatusCode() > 399 {
					ctx.Error("File not found", 404)
					f, err := pkger.Open("/html/404.html")
					if err == nil {
						c, err := ioutil.ReadAll(f)
						if err == nil {
							ctx.Request.Reset()
							ctx.Response.Reset()
							ctx.Response.Header.SetContentType("text/html; charset=utf-8")
							ctx.Response.Header.SetContentLength(len(c))
							_, _ = ctx.Write(c)
						}
					}
				}
				ctx.SetStatusCode(404)
				ctx.Response.Header.Set("Cache-Control", "no-cache")
			}

			// set security headers & Server header
			ctx.Response.Header.Set("Access-Control-Allow-Origin", "*")
			ctx.Response.Header.Set("Referrer-Policy", "strict-origin-when-cross-origin")
			ctx.Response.Header.Set("X-XSS-Protection", "1; mode=block")
			ctx.Response.Header.Set("Server", "Codeberg Pages")
		},
	}).Serve(listener)
}

type disabledLogger struct{}
func (disabledLogger) Printf(format string, args ...interface{}) {}
