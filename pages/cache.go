package pages

import (
	"encoding/json"
	"github.com/buger/jsonparser"
	"github.com/rs/zerolog/log"
	"io/ioutil"
	"net/http"
	"path"
	"strings"
	"sync"
	"time"
)

// buildCache stores the current build process for all pages repositories, and is mostly used for build logs & status information

var buildCache = map[string]*BuildInfo{}
var buildCacheLock = sync.Mutex{}

func LoadCache() error {
	cacheFile, err := ioutil.ReadFile(path.Join(BuildRoot, "build-cache.txt"))
	if err != nil {
		return err
	}
	err = json.Unmarshal(cacheFile, &buildCache)
	if err != nil {
		return err
	}
	return nil
}

func SaveCache() error {
	buildCacheLock.Lock()
	cache := map[string]*BuildInfo{}
	for k := range buildCache {
		if buildCache[k].Done {
			cache[k] = buildCache[k]
		}
	}
	cacheFile, err := json.Marshal(cache)
	buildCacheLock.Unlock()
	if err != nil {
		return err
	}
	return ioutil.WriteFile(path.Join(BuildRoot, "build-cache.txt"), cacheFile, 0644)
}

// hashCache caches the latest commit and is used to check if there are updates or if the repository exists at all

type hashCacheEntry struct {
	Timestamp time.Time
	Value     string
	Branch    string
}
var hashCache = map[string]hashCacheEntry{}

func getJsonValue(url string, path []string) string {
	res, err := http.Get(url)
	if err != nil || res.StatusCode != 200 {
		return ""
	}
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return ""
	}
	value, err := jsonparser.GetString(body, path...)
	if err != nil {
		log.Warn().Str("url", url).Strs("path", path).Err(err).Msg("couldn't parse JSON")
	}
	if err != nil {
		return ""
	}
	return value
}

// HashRepository returns the SHA hash of the commit to use for a pages build, or the empty string if the repository doesn't exist or isn't set up for pages.
func HashRepository(userName, repoName string) (string, string) {
	if hash, ok := hashCache[userName + "/" + repoName]; ok && time.Now().Sub(hash.Timestamp) < 10 * time.Second {
		return hash.Branch, hash.Value
	}

	// check if the repository exists
	defaultBranch := getJsonValue(strings.ReplaceAll(strings.ReplaceAll(RepositoryBranchURL, "%u", userName), "%r", repoName), RepositoryBranchQuery)
	if defaultBranch == "" {
		hashCache[userName + "/" + repoName] = hashCacheEntry{time.Now(), "", ""}
		return "", ""
	}

	hashURL := strings.ReplaceAll(strings.ReplaceAll(RepositoryHashURL, "%u", userName), "%r", repoName)

	// try the "pages" branch
	hash := getJsonValue(strings.ReplaceAll(hashURL, "%b", "pages"), RepositoryHashQuery)
	if hash != "" {
		hashCache[userName + "/" + repoName] = hashCacheEntry{time.Now(), hash, "pages"}
		return "pages", hash
	}

	// try the "gh-pages" branch for compatibility
	hash = getJsonValue(strings.ReplaceAll(hashURL, "%b", "gh-pages"), RepositoryHashQuery)
	if hash != "" {
		hashCache[userName + "/" + repoName] = hashCacheEntry{time.Now(), hash, "gh-pages"}
		return "gh-pages", hash
	}

	// only try the default branch for the pages repo
	if repoName != "pages" {
		hashCache[userName + "/" + repoName] = hashCacheEntry{time.Now(), "", ""}
		return "", ""
	}

	// try the default branch
	hash = getJsonValue(strings.ReplaceAll(hashURL, "%b", defaultBranch), RepositoryHashQuery)
	if hash != "" {
		hashCache[userName + "/" + repoName] = hashCacheEntry{time.Now(), hash, defaultBranch}
		return defaultBranch, hash
	}

	// repo is not set up for pages
	hashCache[userName + "/" + repoName] = hashCacheEntry{time.Now(), "", ""}
	return "", ""
}
