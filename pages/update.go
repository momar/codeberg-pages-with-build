package pages

import (
	"os"
	"path"
	"time"
)

// Update returns the currently running BuildInfo if a build is already running.
// If it's not running or has failed, it checks against RepositoryHashURL if the branch has changed.
// If it has, it schedules a build and returns the new BuildInfo, otherwise it returns nil,
// which means that the cached content is still valid.
func Update(userName, repoName string) *BuildInfo {
	buildCacheLock.Lock()
	defer buildCacheLock.Unlock()

	_, err := os.Stat(path.Join(BuildRoot, "output", userName, repoName))
	exists := !os.IsNotExist(err)

	branch, hash := HashRepository(userName, repoName)
	if hash == "" {
		return nil // repo doesn't exist or isn't set up for pages
	}

	info, ok := buildCache[userName + "/" + repoName]
	if ok && (exists || (info.Error != nil && time.Now().Sub(info.LastCheck) < time.Hour)) && info.Commit == hash {
		info.LastCheck = time.Now()
		return info
	}

	// Need to start a new build
	buildCache[userName + "/" + repoName] = Build(userName, repoName, branch, hash)
	return buildCache[userName + "/" + repoName]
}
