package pages

import (
	"bufio"
	"context"
	"encoding/base32"
	"fmt"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/mount"
	"github.com/docker/docker/api/types/network"
	"github.com/docker/docker/client"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"gopkg.in/src-d/go-billy.v4/osfs"
	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing"
	"gopkg.in/src-d/go-git.v4/plumbing/cache"
	"gopkg.in/src-d/go-git.v4/storage/filesystem"
	"io"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"time"
)

type BuildInfo struct {
	Repository string
	Owner string
	Commit string
	Branch string
	LastCheck time.Time

	Start time.Time
	RequiresBuild *bool
	Error error
	Done bool

	Log string `json:"-"`
	LogLock sync.RWMutex `json:"-"`
	wait sync.RWMutex
}

func (b *BuildInfo) append(format string, a ...interface{}) {
	msg := ""
	if format == "" {
		msg = a[0].(string)
	} else {
		msg = fmt.Sprintf(format, a...)
	}
	msg = strings.TrimSuffix(msg, "\n") + "\n"
	if len(msg) > 0 && (msg[0] == 1 || msg[0] == 2) {
		// docker-encoded (https://godoc.org/github.com/docker/docker/client#Client.ContainerLogs)
		msg = msg[8:]
	}
	b.Log += msg
	b.LogLock.Unlock()
	b.LogLock.Lock()
}
func (b *BuildInfo) throwOnError(desc string, err error) {
	if err == nil {
		return
	}
	b.append(desc + ": " + err.Error())
	b.Error = err
	b.Done = true
	b.LogLock.Unlock()
	b.wait.Unlock()
	panic("throw()")
}

func Build(userName string, repoName string, branch string, commit string) *BuildInfo {
	var info = &BuildInfo{
		LastCheck: time.Now(),
		Start: time.Now(),
		Repository: repoName,
		Owner: userName,
		Commit: commit,
		Branch: branch,
	}
	info.LogLock.Lock()
	info.wait.Lock()
	go func() {
		log.Debug().Str("repository", userName + "/" + repoName).Msg("starting build")
		info.append("cloning repository...")

		// Prepare store path & catch errors
		storePath, err := filepath.Abs(path.Join(BuildRoot, "git", userName, repoName))
		target, err2 := filepath.Abs(path.Join(BuildRoot, "output", userName, repoName))
		defer func() {
			_ = os.RemoveAll(path.Join(storePath, "repo"))
			_ = os.RemoveAll(path.Join(storePath, "input"))
			if r := recover(); r != nil {
				_ = os.RemoveAll(target)
				if r != "throw()" {
					log.Error().Str("repository", userName + "/" + repoName).Interface("error", r).Msg("build failed")
					panic(r)
				} else {
					log.Warn().Str("repository", userName + "/" + repoName).Err(info.Error).Msg("build failed")
				}
			} else {
				log.Info().Str("repository", userName + "/" + repoName).Msg("build succeeded")
			}
		}()
		info.throwOnError("couldn't resolve git filepath", err)
		info.throwOnError("couldn't resolve output filepath", err2)


		// Build the url
		var url = strings.ReplaceAll(strings.ReplaceAll(RepositoryFormat, "%u", userName), "%r", repoName)

		// Choose repository folder (for the Git repo)
		_ = os.RemoveAll(storePath)
		store := filesystem.NewStorage(osfs.New(path.Join(storePath, "repo")), cache.NewObjectLRUDefault())
		err = os.MkdirAll(path.Join(storePath, "repo"), 0755)
		info.throwOnError("couldn't create git dir", err)
		err = os.MkdirAll(path.Join(storePath, "input"), 0755)
		info.throwOnError("couldn't create source dir", err)
		source := osfs.New(path.Join(storePath, "input"))

		// Open or clone the repository
		var repo *git.Repository
		repo, err = git.Open(store, source)
		if err != nil { // can't open repository, try to clone
			repo, err = git.Clone(store, source, &git.CloneOptions{
				URL: url,
				NoCheckout: true,
				Depth: 1,
				SingleBranch: true,
				ReferenceName: plumbing.NewBranchReferenceName(info.Branch),
			})
			info.throwOnError("couldn't clone repository", err)
		}
		tree, err := repo.Worktree()
		info.throwOnError("couldn't retrieve worktree", err)

		err = tree.Checkout(&git.CheckoutOptions{
			Hash: plumbing.NewHash(commit),
		})
		info.throwOnError("couldn't checkout commit", err)

		// Select the correct build tool
		var buildImage string
		var buildCommand []string
		var move = false
		var exists = func(p string) bool {
			_, err := source.Stat(p)
			return err == nil
		}
		if (exists("_config.yml") || exists("_config.toml")) && (exists("index.html") || exists("index.md")) {
			// Jekyll
			buildImage = "docker.io/jekyll/builder"
			buildCommand = []string{"jekyll", "build", "-d", "/output", "--safe"}
			// TODO: create an image with preinstalled commonly used gems
			// TODO: force relative links?
		} else if (exists("config.toml") || exists("config.yaml") || exists("config.json")) && exists("content") {
			// Hugo
			// TODO: create an image (automatically updated)
			// TODO: force relative links?
		} else if exists("mkdocs.yml") {
			// MkDocs
			buildImage = "docker.io/squidfunk/mkdocs-material"
			buildCommand = []string{"build", "--site-dir", "/output"}
			// TODO: force relative links?
		} else if exists("index.html") {
			move = true
		} else if repoName == "pages" {
			// Repositories called pages don't need an index.html file.
			move = true
		} else {
			info.throwOnError("pages aren't set up for this repository", errors.New("couldn't determine build tool"))
		}

		rb := !move
		info.RequiresBuild = &rb
		info.append("updating pages... %s %v", buildImage, buildCommand)

		// Build the image
		err = os.RemoveAll(target)
		if !os.IsNotExist(err) {
			info.throwOnError("couldn't cleanup output directory", err)
		}

		var status int64 = 0
		if !move {
			err = os.MkdirAll(target, 0755)
			info.throwOnError("couldn't create output directory", err)

			docker, err := client.NewEnvClient()
			info.throwOnError("couldn't set up docker client", err)

			// pull image
			if _, err = docker.ImageHistory(context.Background(), buildImage); err != nil {
				imagePull, _ := docker.ImagePull(context.Background(), buildImage, types.ImagePullOptions{})
				if imagePull != nil {
					_, _ = ioutil.ReadAll(imagePull)
				}
			}

			c, _ := context.WithTimeout(context.Background(), BuildTimeout)
			containerName := "pages_" + base32.StdEncoding.WithPadding(base32.NoPadding).EncodeToString([]byte(userName+"/"+repoName))

			// remove container if it exists
			_ = docker.ContainerRemove(context.Background(), containerName, types.ContainerRemoveOptions{Force: true})

			cont, err := docker.ContainerCreate(c, &container.Config{
				Cmd:   buildCommand,
				Image: buildImage,
				WorkingDir: "/input",
				User: strconv.Itoa(os.Geteuid()),
			}, &container.HostConfig{
				Mounts: []mount.Mount{
					{
						Type:     "bind",
						Source:   path.Join(storePath, "input"),
						Target:   "/input",
						ReadOnly: false,
					},
					{
						Type:     "bind",
						Source:   target,
						Target:   "/output",
						ReadOnly: false,
					},
				},
			}, &network.NetworkingConfig{}, containerName)
			info.throwOnError("couldn't create container", err)
			var cleanupOnError = func(err error) {
				if err != nil {
					_ = docker.ContainerRemove(context.Background(), cont.ID, types.ContainerRemoveOptions{Force: true})
					_ = os.RemoveAll(path.Join(storePath, "repo"))
					_ = os.RemoveAll(path.Join(storePath, "input"))
				}
			}

			err = docker.ContainerStart(c, cont.ID, types.ContainerStartOptions{})
			cleanupOnError(err)
			info.throwOnError("couldn't start container", err)

			logs, err := docker.ContainerLogs(c, cont.ID, types.ContainerLogsOptions{ShowStderr: true, ShowStdout: true, Follow: true})
			cleanupOnError(err)
			info.throwOnError("couldn't retrieve container logs", err)

			r := bufio.NewReader(logs)
			line := ""
			for err == nil {
				line, err = r.ReadString('\n')
				if err != nil && err != io.EOF {
					cleanupOnError(err)
					info.throwOnError("couldn't read container logs", err)
				}
				if err == nil {
					info.append("", line)
				}
			}

			status, err = docker.ContainerWait(c, cont.ID)
			cleanupOnError(err)
			info.throwOnError("couldn't wait for container", err)

			err = docker.ContainerRemove(context.Background(), cont.ID, types.ContainerRemoveOptions{Force: true})
			_ = os.RemoveAll(path.Join(storePath, "repo"))
			_ = os.RemoveAll(path.Join(storePath, "input"))
			info.throwOnError("couldn't remove container", err)

		} else {
			// Move only, don't use Docker at all
			err = os.MkdirAll(filepath.Dir(target), 0755)
			info.throwOnError("couldn't create output directory", err)
			err = os.Rename(path.Join(storePath, "input"), target)
			info.throwOnError("couldn't move sources", err)
		}

		if status != 0 {
			info.throwOnError("build failed", fmt.Errorf("exit code %d", status))
		} else {
			info.append("build succeeded")
			info.Done = true
			info.LogLock.Unlock()
			info.wait.Unlock()

			if err := SaveCache(); err != nil {
				log.Warn().Err(err).Msg("couldn't save build cache")
			} else {
				log.Debug().Msgf("build cache saved (currently %d repositories)", len(buildCache))
			}
		}
	}()
	return info
}
