package pages

import (
	"github.com/rs/zerolog/log"
	"os"
	"path"
	"path/filepath"
	"time"
)

// Cleanup checks all cached contents & cleans up the output paths of repositories which aren't readable anymore.
// It should be regularly executed (e.g. every 15 minutes), to ensure that setting repos to private or deleting
// them is effective relatively quickly, and of course to save disk space.
func Cleanup() {
	now := time.Now()

	for k, v := range hashCache {
		if now.Sub(v.Timestamp) > 10 * time.Second {
			delete(hashCache, k)
		}
	}

	repos, err := filepath.Glob(path.Join(BuildRoot, "output") + "/*/*")
	if err != nil {
		log.Warn().Err(err).Msg("bad glob pattern for cleanup")
		return
	}
	if repos == nil || len(repos) == 0 {
		log.Warn().Err(err).Msg("no built repositories found for cleanup")
		repos = []string{}
	}

	for _, repo := range repos {
		repoName := filepath.Base(repo)
		userName := filepath.Base(filepath.Dir(repo))

		// Delete all targets for which no build cache exists or where it threw an error more than 60 minutes ago
		if info, ok := buildCache[userName + "/" + repoName]; !ok || (info.Done && info.Error != nil && now.Sub(info.Start) > 60 * time.Minute) {
			err := os.RemoveAll(repo)
			if err != nil {
				log.Warn().Str("repository", userName + "/" + repoName).Err(err).Msg("repository is orphaned & couldn't be cleaned up")
			} else {
				log.Info().Str("repository", userName + "/" + repoName).Msg("repository is orphaned, cleaned up")
			}
			continue
		}

		// Delete all targets for which no repository exists
		if _, hash := HashRepository(userName, repoName); hash == "" {
			err := os.RemoveAll(repo)
			if err != nil {
				log.Warn().Str("repository", userName + "/" + repoName).Err(err).Msg("repository is inaccessible & couldn't be cleaned up")
			} else {
				log.Info().Str("repository", userName + "/" + repoName).Msg("repository is inaccessible, cleaned up")
			}
		}
	}

	// Delete all cached builds which are marked as "Done" but don't have a corresponding directory
	buildCacheLock.Lock()
	for repo := range buildCache {
		if buildCache[repo].Done && buildCache[repo].Error == nil {
			_, err := os.Stat(path.Join(BuildRoot, "output", buildCache[repo].Owner, buildCache[repo].Repository))
			if err != nil && os.IsNotExist(err) {
				log.Info().Str("repository", repo).Msg("build files are missing, removing build cache")
				delete(buildCache, repo)
			}
		}
	}
	buildCacheLock.Unlock()

	// Delete all build files for which the info says that they're done
	buildFiles, err := filepath.Glob(path.Join(BuildRoot, "git") + "/*/*")
	if err != nil {
		log.Warn().Err(err).Msg("bad glob pattern for build file cleanup")
		return
	}
	for _, repo := range buildFiles {
		repoName := filepath.Base(repo)
		userName := filepath.Base(filepath.Dir(repo))
		if info, ok := buildCache[repoName + "/" + userName]; !ok || (info.Done && now.Sub(info.Start) > 60 * time.Minute) {
			err := os.RemoveAll(repo)
			if err != nil {
				log.Warn().Str("repository", userName + "/" + repoName).Err(err).Msg("repository left over build files & couldn't be cleaned up")
			} else {
				log.Info().Str("repository", userName + "/" + repoName).Msg("repository left over build files, cleaned up")
			}
		}
	}
}
