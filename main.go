package main

import (
	"codeberg.org/Codeberg/pages/pages"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

func main() {
	log.Logger = zerolog.New(zerolog.NewConsoleWriter())
	pages.BuildRoot = "./temp"
	pages.Domain = "localhost:8000"
	pages.Setup()
	log.Error().Err(pages.Serve()).Msg("server stopped")
	return
}
